fn main() {
    // Creating a new vector - explicit type annotation
    let v: Vec<i32> = Vec::new();

    // Creating a vector with macro - type inference
    let v = vec![1, 2, 3];

    println!("{:?}", v);

    // Updating a vector
    let mut v = Vec::new();

    // Commenting this would give `cannot infer type for T`
    // Type inference works when we add some value to it
    v.push(5);
    v.push(6);
    v.push(7);

    println!("{:?}", v);

    {
        // v goes out of scope after this block
        // all elements are dropped
        // gets tricky when elements have reference
        let v = vec![1, 2, 3, 4];

        println!("v doesn't exist after here: {:?}", v);
    }

    println!("{:?}", v);

    // Reading elements
    let v = vec![1, 2, 3, 4, 5];

    let third: &i32 = &v[2];
    println!("third {}", third);

    let third: Option<&i32> = v.get(2);
    println!("third {}", third.unwrap());

    // Causes the program to panic
    let hundredth: &i32 = &v[100];

    // Returns None if an index is out of bounds
    let hundredth: Option<&i32> = v.get(100);

    // Can't have immutable & mutable borrow
    // in same scope
    let mut v = vec![1, 2, 3 , 4, 5];

    // Immutable borrow
    let first = &v[0];
    // Mutable borrow
    v.push(6);

    // Iterating vector
    for i in &v {
        println!("{}", i);
    }

    // Iterating and mutating vector
    for i in &mut v {
        // Dereference using *
        *i += 10;
        println!("{}", i);
    }

    // Using enum to store multiple types
    #[derive(Debug)]
    enum SpreadSheetcell {
        Int(i32),
        Float(f64),
        Text(String)    
    }

    let row = vec![
        SpreadSheetcell::Int(314),
        SpreadSheetcell::Float(3.14),
        SpreadSheetcell::Text(String::from("PI"))
    ];

    println!("{:?}", row);
}
